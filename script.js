// console.log("hello Elliot");

const url = 'https://jsonplaceholder.typicode.com/todos/';

fetch(url)
	.then((response) => response.json())
	.then((json) => console.log(json));

	const listArray = [];
	fetch(url)
	.then((response) => response.json())
	.then((json) => {
		json.map((elem) => {
			listArray.push(elem.title);
		});
	});
	console.log(listArray);


	fetch(url+"/1")
	.then((response) => response.json())
	.then((json) => console.log(json));


	fetch(url+"/1")
	.then((response) => response.json())
	.then((json) => console.log(`The item ${json.title} on the list has a status of ${json.completed}`));


	fetch(url, {
	method: 'POST',
	headers: {
			'Content-type': 'application/json',
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	
	async function fetchData(){
	fetch(url+'/1', {
		method: 'PUT',
		headers: {
	  	'Content-type': 'application/json',
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));
	}
	fetchData();


	fetch(url+ '/1', {
		method: 'PUT',
		headers: {
	  	'Content-type': 'application/json',
		},
		body: JSON.stringify({
			completed: false,
			dateCompleted: "11/29/22",
			id: 1,
			status: "Completed",
			title: "delectus aut autem",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	fetch(url+'/1', {
		method: 'PATCH',
		body: JSON.stringify({
			completed: true,
			title:"This is a patched To do list item"
			}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
			}
		})
	.then(response => response.json())
	.then(json => console.log(json));


	fetch(url+'/1', {
		method: 'PATCH',
		body: JSON.stringify({
			completed: true,
			dateCompleted: "11/29/22"
			}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
			}
		})
	.then(response => response.json())
	.then(json => console.log(json));

	

	fetch(url+'/1', {
	  method: 'DELETE'
	});
